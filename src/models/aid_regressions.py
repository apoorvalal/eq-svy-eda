
# coding: utf-8

# In[147]:


#%%
import os
import sys
import glob
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

import statsmodels.api as sm
import statsmodels.formula.api as smf
import linearmodels as lm
from linearmodels.iv import IV2SLS

get_ipython().run_line_magic('matplotlib', 'inline')
# run for jupyter notebook
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'
#%%


# ## Read data

# In[148]:


working = '/home/alal/Desktop/code/0_research/eq-svy-eda'
get_ipython().run_line_magic('cd', '$working')
get_ipython().run_line_magic('time', "hh_all = pd.read_csv(working+'/data/interim/hh_with_geo.csv')")


# In[149]:


hh_all.columns


# In[150]:


hh_all.has_death_occurred_last_12_months.value_counts()


# In[151]:


hh_all['post_eq_effects']=hh_all[['has_death_occurred_last_12_months',
                                  'has_injury_loss_occurred_last_12_months',
                                  'has_education_drop_occurred_last_12_months',
                                  'has_pregnancy_treatment_drop_occurred_last_12_months',
                                  'has_vaccination_drop_occurred_last_12_months',
                                  'has_occupation_change_occurred_last_12_months']].max(axis=1)


# In[152]:


hh_all['post_eq_effects'].value_counts()


# In[153]:


hh_all['aid']=hh_all[['is_recipient_rahat_15k',
                      'is_recipient_rahat_10k', 'is_recipient_rahat_200k']].max(axis=1)


# In[154]:


hh_all['aid'].value_counts()


# construct aggregated caste groups

# In[155]:


bcn_mapping = {
'Brahman-Hill': 'BCN',
'Brahman-Tarai': 'BCN',
'Chhetree': 'BCN',
'Newar': 'BCN',
'Thakuri': 'BCN',
'Majhi': 'non-BCN',
'Rai': 'non-BCN',
'Kami': 'non-BCN',
'Magar': 'non-BCN',
'Damai/Dholi': 'non-BCN',
'Limbu': 'non-BCN',
'Sarki': 'non-BCN',
'Yakkha': 'non-BCN',
'Tamang': 'non-BCN',
'Gharti/Bhujel': 'non-BCN',
'Sanyasi/Dashnami': 'non-BCN',
'Gurung': 'non-BCN',
'0thers': 'non-BCN',
'Khawas': 'non-BCN',
'Teli': 'non-BCN',
'Kumal': 'non-BCN',
'Byasi/Sauka': 'non-BCN',
'Sherpa': 'non-BCN',
'Mewahang Bala': 'non-BCN',
'Kulung': 'non-BCN',
'Jhangad/Dhagar': 'non-BCN',
'Lohar': 'non-BCN',
'Bhote': 'non-BCN',
'Sunuwar': 'non-BCN',
'Ghale': 'non-BCN',
'Thulung': 'non-BCN',
'Khaling': 'non-BCN',
'Chamling': 'non-BCN',
'Thami': 'non-BCN',
'Gaderi/Bhedhar': 'non-BCN',
'Sonar': 'non-BCN',
'Hayu': 'non-BCN',
'Gaine': 'non-BCN',
'Musalman': 'non-BCN',
'Pahari': 'non-BCN',
'Jirel': 'non-BCN',
'Baraee': 'non-BCN',
'Kurmi': 'non-BCN',
'Nachhiring': 'non-BCN',
'Rajbansi': 'non-BCN',
'Yadav': 'non-BCN',
'Brahmu/Baramo': 'non-BCN',
'Raute': 'non-BCN',
'Tharu': 'non-BCN',
'Dhanuk': 'non-BCN',
'Danuwar': 'non-BCN',
'Bin': 'non-BCN',
'Chepang/Praja': 'non-BCN',
'Hajam/Thakur': 'non-BCN',
'Darai': 'non-BCN',
'Kumhar': 'non-BCN',
'Kanu': 'non-BCN',
'Rajput': 'non-BCN',
'Chhantyal/Chhantel': 'non-BCN',
'Thakali': 'non-BCN',
'Kayastha': 'non-BCN',
'Kalwar': 'non-BCN',
'Tatma/Tatwa': 'non-BCN',
'Badhaee': 'non-BCN',
'Bote': 'non-BCN',
'Sudhi': 'non-BCN',
'Chamar/Harijan/Ram': 'non-BCN',
'Musahar': 'non-BCN',
'Dusadh/Pasawan/Pasi': 'non-BCN',
'Kamar': 'non-BCN',
'Satar/Santhal': 'non-BCN',
'Dura': 'non-BCN',
'Gangai': 'non-BCN',
'Marwadi': 'non-BCN',
'Dom': 'non-BCN',
'Kisan': 'non-BCN',
'Loharung': 'non-BCN',
'Mallaha': 'non-BCN',
'Kewat': 'non-BCN',
'Koiri/Kushwaha': 'non-BCN',
'Kahar': 'non-BCN',
'Kusunda': 'non-BCN',
'Lepcha': 'non-BCN',
'Mali': 'non-BCN',
'Nuniya': 'non-BCN',
'Dhobi': 'non-BCN',
'Punjabi/Shikh': 'non-BCN',
'Hyolmo': 'non-BCN',
'Khatwe': 'non-BCN',
'Haluwai': 'non-BCN',
'Amat': 'non-BCN',
'Kori': 'non-BCN',
'Badi': 'non-BCN',
'Bantar/Sardar': 'non-BCN',
'Kalar': 'non-BCN',
'Dhimal': 'non-BCN',
'Meche': 'non-BCN',
'Lhopa': 'non-BCN',
'Koche': 'non-BCN',
'Kathbaniyan': 'non-BCN',
'Dev': 'non-BCN',
'Bahing': 'non-BCN',
'Bangali': 'non-BCN',
'Pattharkatta/Kushwadiya': 'non-BCN',
'Samgpang': 'non-BCN',
'Natuwa':'non-BCN'
}


# In[156]:


hh_all['caste_general']=hh_all.caste_household.map(bcn_mapping)
hh_all['caste_general'].value_counts()


# In[157]:


# village houeshold counts
hh_all['village_counts']=hh_all.groupby('vdcmun_id').household_id.transform('count')
# tag BCN households
hh_all['bcn']=hh_all.caste_general=='BCN'
# village BCN aggregates
hh_all['village_bcn_counts']=hh_all.groupby('vdcmun_id').bcn.transform('sum')
# village BCN share
hh_all['bcn_share']=hh_all.village_bcn_counts / hh_all.village_counts


# In[158]:


hh_all.head()


# ## diagnostic plots

# In[159]:


ct = pd.crosstab(hh_all.post_eq_effects, hh_all.aid)

ct.plot.bar(stacked=True)
plt.legend(title='mark')

plt.show()


# In[160]:


hh_all.groupby('vdcmun_id').bcn_share.agg('first').describe().T
sns.distplot(hh_all.groupby('vdcmun_id').bcn_share.agg('first'))


# In[161]:


sns.regplot(x=hh_all.groupby('vdcmun_id').village_counts.agg('first'),
            y=hh_all.groupby('vdcmun_id').village_bcn_counts.agg('first'))


# ## OLS Regressions

# In[162]:


ols_endog = smf.ols('post_eq_effects ~ aid + size_household',data=hh_all).fit(cov_type='HC0')
print(ols_endog.summary())


# In[163]:


first_stage = smf.ols("aid ~ bcn_share + size_household", data=hh_all).fit(cov_type='HC0')
print(first_stage.summary())


# In[164]:


reduced_form = smf.ols('post_eq_effects ~ bcn_share + size_household',data=hh_all).fit(cov_type='HC0')
print(reduced_form.summary())


# ## IV Estimation

# In[165]:


iv = IV2SLS.from_formula('post_eq_effects ~ 1 + size_household + [aid ~ bcn_share]',
                         data=hh_all).fit(cov_type='robust')
print(iv.summary)


# OLS $\partial N / \partial r > 0$ (i.e. More aid, more bad outcomes) - strong selection bias

# In[166]:


ols_endog.params[1]


# IV $\partial N / \partial r < 0$ (i.e. more aid, fewer bad outcomes)

# In[167]:


iv.params[2]
