
# coding: utf-8

# In[1]:


#%%
import os
import sys
import glob
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
# run for jupyter notebook
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'
#%%


# In[2]:


working = '/home/alal/Desktop/code/eq-svy-eda'
get_ipython().run_line_magic('cd', '$working')
get_ipython().run_line_magic('time', "hh_all = pd.read_csv(working+'/data/interim/hh_with_geo.csv')")


# In[3]:


hh_all.shape


# In[4]:


hh_all.head()


# ## Summary stats for categorical variables

# In[6]:


def return_counts(vname,df='hh_all'):
    return df[vname].value_counts()


# In[7]:


income_counts = return_counts('income_level_household', hh_all)
income_counts


# In[8]:


hh_chars = [
    'is_bank_account_present_in_household',
    'income_level_household',
    'household_eq_id_type',
    'education_level_household_head',
    'gender_household_head'
]

eq_effs = [
    'count_death_last_12_months',
    'count_education_drop_last_12_months',
    'count_injury_loss_last_12_months',
    'count_occupation_change_last_12_months',
    'count_pregnancy_treatment_drop_last_12_months',
    'count_vaccination_drop_last_12_months',
    'has_death_occurred_last_12_months',
    'has_education_drop_occurred_last_12_months',
    'has_injury_loss_occurred_last_12_months',
    'has_occupation_change_occurred_last_12_months',
    'has_pregnancy_treatment_drop_occurred_last_12_months',
    'has_vaccination_drop_occurred_last_12_months'
]

pre_post = [
    'has_asset_cable_post_eq', 
    'has_asset_cable_pre_eq', 
    'has_asset_computer_post_eq', 
    'has_asset_computer_pre_eq',
    'has_asset_four_wheeler_commercial_post_eq', 
    'has_asset_four_wheeler_commercial_use_pre_eq', 
    'has_asset_four_wheeler_family_use_post_eq', 
    'has_asset_four_wheeler_family_use_pre_eq', 
    'has_asset_fridge_post_eq',
    'has_asset_fridge_pre_eq',
    'has_asset_internet_post_eq',
    'has_asset_internet_pre_eq',
    'has_asset_land_post_eq',
    'has_asset_land_pre_eq',
    'has_asset_mobile_phone_post_eq',
    'has_asset_mobile_phone_pre_eq',
    'has_asset_motorcycle_post_eq',
    'has_asset_motorcycle_pre_eq',
    'has_asset_none_post_eq'
    'has_asset_none_pre_eq',
    'has_asset_telephone_post_eq',
    'has_asset_telephone_pre_eq',
    'has_asset_tv_post_eq',
    'has_asset_tv_pre_eq',
    'source_cooking_fuel_post_eq',
    'source_cooking_fuel_pre_eq',
    'source_light_post_eq',
    'source_light_pre_eq',
    'source_water_post_eq',
    'source_water_pre_eq',
    'type_toilet_post_eq',
    'type_toilet_pre_eq',
    'residence_district_household_head_post_eq',
    'residence_district_household_head_pre_eq',
    'residence_household_post_eq',
    'residence_household_pre_eq',
]


assist = [
    'is_ineligible_rahat',
    'is_recipient_rahat_10k',
    'is_recipient_rahat_15k',
    'is_recipient_rahat_200k',
    'is_recipient_rahat_none',
    'is_recipient_rahat_social_security_3k'
]


# In[9]:


hh_char_counts = [return_counts(v,hh_all) for v in hh_chars]
len(hh_char_counts)


# In[10]:


eq_effs_counts = [return_counts(v,hh_all) for v in eq_effs]
len(eq_effs_counts)


# In[11]:


assist_counts = [return_counts(v,hh_all) for v in assist]
len(assist_counts)


# In[44]:


hh_char_counts[1]


# In[45]:


total =  hh_char_counts[1].sum() 
fig, ax = plt.subplots()   
hh_char_counts[1].plot(kind='barh',stacked=True,title=hh_chars[1] + '\n' +
                      'N='+str(total))
plt.tight_layout()
plt.xticks(rotation=90)
i = 0
for v in hh_char_counts[1].iteritems():  
    ax.text(500, i-.2, str(v[1]))
    i += 1


# In[46]:


plot_out= '/home/alal/Desktop/code/eq-svy-eda/reports/figures'


# In[52]:


get_ipython().run_cell_magic('capture', '', "for i in range(len(eq_effs_counts)):\n    total =  eq_effs_counts[i].sum() \n    eq_effs_counts[i].plot(kind='barh',stacked=True,title=eq_effs[i] + '\\n' +\n                          'N='+str(total))\n    plt.tight_layout()\n    plt.xticks(rotation=90)\n    plt.savefig(plot_out+'/'+eq_effs[i]+'.png', dpi = 300)")


# In[53]:


get_ipython().run_cell_magic('capture', '', "for i in range(len(hh_char_counts)):\n    total =  hh_char_counts[i].sum() \n    hh_char_counts[i].plot(kind='barh',stacked=True,title=hh_chars[i] + '\\n' +\n                          'N='+str(total))\n    plt.tight_layout()\n    plt.xticks(rotation=90)\n    plt.savefig(plot_out+'/'+hh_chars[i]+'.png', dpi = 300)")


# In[54]:


get_ipython().run_cell_magic('capture', '', "for i in range(len(assist_counts)):\n    total =  assist_counts[i].sum() \n    assist_counts[i].plot(kind='barh',stacked=True,title=assist[i] + '\\n' +\n                          'N='+str(total))\n    plt.tight_layout()\n    plt.xticks(rotation=90)\n    plt.savefig(plot_out+'/'+assist[i]+'.png', dpi = 300)")


# ## Pre-post comparison - assets

# In[135]:


hh_all['has_asset_cable_diff'] = hh_all['has_asset_cable_pre_eq'] - hh_all['has_asset_cable_post_eq']
hh_all['has_asset_computer_diff'] = hh_all['has_asset_computer_pre_eq']- hh_all['has_asset_computer_post_eq']
hh_all['has_asset_four_wheeler_commercial_use_diff'] = hh_all['has_asset_four_wheeler_commercial_use_pre_eq']- hh_all['has_asset_four_wheeler_commercial_post_eq']
hh_all['has_asset_four_wheeler_family_use_diff'] = hh_all['has_asset_four_wheeler_family_use_pre_eq']- hh_all['has_asset_four_wheeler_family_use_post_eq']
hh_all['has_asset_fridge_diff'] = hh_all['has_asset_fridge_pre_eq']- hh_all['has_asset_fridge_post_eq']
hh_all['has_asset_internet_diff'] = hh_all['has_asset_internet_pre_eq']- hh_all['has_asset_internet_post_eq']
hh_all['has_asset_land_diff'] = hh_all['has_asset_land_pre_eq']- hh_all['has_asset_land_post_eq']
hh_all['has_asset_mobile_phone_diff'] = hh_all['has_asset_mobile_phone_pre_eq']- hh_all['has_asset_mobile_phone_post_eq']
hh_all['has_asset_motorcycle_diff'] = hh_all['has_asset_motorcycle_pre_eq']- hh_all['has_asset_motorcycle_post_eq']
hh_all['has_asset_none_diff'] = hh_all['has_asset_none_pre_eq']- hh_all['has_asset_none_post_eq']
hh_all['has_asset_telephone_diff'] = hh_all['has_asset_telephone_pre_eq']- hh_all['has_asset_telephone_post_eq']
hh_all['has_asset_tv_diff'] = hh_all['has_asset_tv_pre_eq']- hh_all['has_asset_tv_post_eq']
hh_all['source_cooking_fuel_diff'] = hh_all['source_cooking_fuel_pre_eq'] == hh_all['source_cooking_fuel_post_eq']
hh_all['source_light_diff'] = hh_all['source_light_pre_eq'] == hh_all['source_light_post_eq']
hh_all['source_water_diff'] = hh_all['source_water_pre_eq'] == hh_all['source_water_post_eq']
hh_all['type_toilet_diff'] = hh_all['type_toilet_pre_eq'] == hh_all['type_toilet_post_eq']
hh_all['residence_district_household_head_diff'] = hh_all['residence_district_household_head_pre_eq'] == hh_all['residence_district_household_head_post_eq']
hh_all['residence_household_diff'] = hh_all['residence_household_pre_eq'] == hh_all['residence_household_post_eq']


# In[137]:


diffs = ['household_id','has_asset_cable_diff', 'has_asset_computer_diff', 'has_asset_four_wheeler_commercial_use_diff',
         'has_asset_four_wheeler_family_use_diff', 'has_asset_fridge_diff', 'has_asset_internet_diff', 
         'has_asset_land_diff', 'has_asset_mobile_phone_diff', 'has_asset_motorcycle_diff', 'has_asset_none_diff',
         'has_asset_telephone_diff', 'has_asset_tv_diff', 'source_cooking_fuel_diff', 'source_light_diff',
         'source_water_diff', 'type_toilet_diff', 'residence_district_household_head_diff', 
         'residence_household_diff']
hh_diffs=hh_all[diffs]

hh_diffs.head()

