# pyscience imports
import os
import sys
import glob
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
# plt.style.use("seaborn-darkgrid")
sns.set(style="ticks", context="talk")
plt.style.use("dark_background")
import statsmodels.api as sm
import statsmodels.formula.api as smf
# %matplotlib inline
# run for jupyter notebook
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'
#%%


# In[4]:

root = '/home/alal/Desktop/code/0_research/eq-svy-eda'
get_ipython().run_line_magic('cd', '$root')
raw = '/media/alal/LAL_DATA/Data/Surveys_Censuses_Misc/Nepal/Nepal-eq-survey/Nepal-eq-NPC-full-survey'


# # Data Prep

# In[6]:


households = pd.read_csv(root+'/data/interim/hh_with_geo.csv')
# households.info()


# In[12]:

buildings_main = pd.read_csv(raw+'/csv_building_structure.csv')
buildings_use  = pd.read_csv(raw+'/csv_building_ownership_and_use.csv')
buildings_damage = pd.read_csv(raw+'/csv_building_damage_assessment.csv')
# In[17]:


stackedXwalk = pd.read_csv(raw+'/crosswalks/00_all_xwalk.csv')
stackedXwalk.head()


# ## Merge

# In[18]:


buildings = buildings_main.merge(buildings_use,
                                 on=['district_id','vdcmun_id','ward_id','building_id'],
                                 how='left')


# In[21]:

buildings = buildings.merge(buildings_damage,
                                 on=['district_id','vdcmun_id','ward_id','building_id'],
                                 how='left')
#%%
buildings = buildings.merge(stackedXwalk,on=['vdcmun_id'],
                            how='left')
# In[22]:


buildings.head()


# In[23]:


buildings.to_csv(root+'/data/interim/buildings.csv')
