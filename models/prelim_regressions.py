#%%
import os
import sys
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.api as sm
import statsmodels.formula.api as smf
# %matplotlib inline
# run for jupyter notebook
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'
#%%
dta = pd.read_csv(
    '/home/alal/Desktop/code/eq-svy-eda/data/interim/hh_merged.csv')
#%%
dta.info()
dta.columns
#%%
# Index(['household_id', 'district_id', 'vdcmun_id', 'ward_id',
#        'gender_household_head', 'age_household_head', 'caste_household',
#        'education_level_household_head', 'income_level_household',
#        'size_household', 'is_bank_account_present_in_household', 'id',
#        'shelter_condition_household_post_eq', 'residence_household_pre_eq',
#        'residence_household_post_eq', 'household_eq_id_type',
#        'has_death_occurred_last_12_months', 'count_death_last_12_months',
#        'has_injury_loss_occurred_last_12_months',
#        'count_injury_loss_last_12_months',
#        'has_education_drop_occurred_last_12_months',
#        'count_education_drop_last_12_months',
#        'has_pregnancy_treatment_drop_occurred_last_12_months',
#        'count_pregnancy_treatment_drop_last_12_months',
#        'has_vaccination_drop_occurred_last_12_months',
#        'count_vaccination_drop_last_12_months',
#        'has_occupation_change_occurred_last_12_months',
#        'count_occupation_change_last_12_months',
#        'residence_district_household_head_pre_eq',
#        'residence_district_household_head_post_eq', 'is_recipient_rahat_15k',
#        'is_recipient_rahat_10k', 'is_recipient_rahat_200k',
#        'is_recipient_rahat_social_security_3k', 'is_recipient_rahat_none',
#        'is_ineligible_rahat', 'source_water_pre_eq', 'source_water_post_eq',
#        'source_cooking_fuel_pre_eq', 'source_cooking_fuel_post_eq',
#        'source_light_pre_eq', 'source_light_post_eq', 'type_toilet_pre_eq',
#        'type_toilet_post_eq', 'has_asset_land_pre_eq', 'has_asset_tv_pre_eq',
#        'has_asset_cable_pre_eq', 'has_asset_computer_pre_eq',
#        'has_asset_internet_pre_eq', 'has_asset_telephone_pre_eq',
#        'has_asset_mobile_phone_pre_eq', 'has_asset_fridge_pre_eq',
#        'has_asset_motorcycle_pre_eq',
#        'has_asset_four_wheeler_family_use_pre_eq',
#        'has_asset_four_wheeler_commercial_use_pre_eq', 'has_asset_none_pre_eq',
#        'has_asset_land_post_eq', 'has_asset_tv_post_eq',
#        'has_asset_cable_post_eq', 'has_asset_computer_post_eq',
#        'has_asset_internet_post_eq', 'has_asset_telephone_post_eq',
#        'has_asset_mobile_phone_post_eq', 'has_asset_fridge_post_eq',
#        'has_asset_motorcycle_post_eq',
#        'has_asset_four_wheeler_family_use_post_eq',
#        'has_asset_four_wheeler_commercial_post_eq', 'has_asset_none_post_eq'],
#       dtype='object')
#%%
dta['rahat'] = (dta.is_recipient_rahat_10k + dta.is_recipient_rahat_200k +
                dta.is_recipient_rahat_social_security_3k) > 0
dta.rahat.value_counts()
#%%
dta.count_education_drop_last_12_months.value_counts(dropna=False)
dta.count_education_drop_last_12_months.fillna(0, inplace=True)
#%%
mod = smf.ols('count_education_drop_last_12_months ~ rahat', data=dta).fit()
#%%
mod.summary()
#%%
