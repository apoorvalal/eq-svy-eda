# pyscience imports
import os
import sys
import glob
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import seaborn as sns
# plt.style.use("seaborn-darkgrid")
sns.set(style="ticks", context="talk")
plt.style.use("dark_background")
import statsmodels.api as sm
import statsmodels.formula.api as smf
# %matplotlib inline
# run for jupyter notebook
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'
#%%

working = '/home/alal/Desktop/code/0_research/eq-svy-eda'
dataroot = os.path.join(working,'data/interim/')
maproot = '/home/alal/Desktop/code/0_research/eq-svy-eda/reports/maps'
get_ipython().run_line_magic('cd', '$working')
get_ipython().run_line_magic('time', "hh_all = pd.read_csv(working+'/data/interim/hh_with_geo.csv')")
#%%


hh_all['vdc']=hh_all['VDC-Municipality-Name'].str.lower()
main_dists = hh_all.loc[hh_all.FullSample==1].District_Name.unique()
main_dists
#%%
main_dists_eq = hh_all.loc[hh_all['District_Name'].isin(main_dists)]
#%%
remaps = {
    "aaru arbang": "aaruaarbad",
    "aaru chanaute": "aaruaarbad",
    "aarupokhari": "aaruaarbad",
    "agara": "agra",
    "alampu": "alambu",
    "ambote": "shikhar ambote",
    "atarpur": "attarpur",
    "badegau": "bandegaun",
    "bageswori": "bageshwari",
    "baksa": "baksha",
    "baldthali": "balthali",
    "balting": "walting",
    "bamti bhandar": "baluwa pati naldhun",
    "baramchi": "baramchae",
    "barpak": "warpak",
    "baruneshwor": "barudeshwor",
    "bhaise": "bhainse",
    "bharta pundyadevi": "bhartapunyadevi",
    "bhedapu": "bhedpu",
    "bhimeshwor municipality": "bhimeswor municipality",
    "bhimfedi": "bhimphedi",
    "bhotasipa": "bhotsipa",
    "bhote namlang": "bhotenamlang",
    "bhujee": "bhuji",
    "bhumesthan": "bhumisthan",
    "bhusafeda": "bhusaphedi",
    "bhussinga": "bhushanga",
    "bitijor bagaiya": "bitijor bagaincha",
    "bocha": "boch",
    "bolde fediche": "boldephadiche",
    "bridhim": "briddim",
    "chalal ganeshsthan": "chalalganeshsthan",
    "chankhu": "changkhu",
    "chaughada": "chaughoda",
    "chautara n.p.": "chautara",
    "chhatredeurali": "chhatre deurali",
    "chilankha": "chilangkha",
    "choubas": "chaubas",
    "chumchet": "chunchet",
    "chyasing kharka": "chyasingkharka",
    "bhadrutar": "bhadratar",
    "baluwapati deupur": "baluwadeubhumi",
    "bekhsimle ghartigaun": "bekhsimle ghartigaon",
    "banakhu chor": "wanakhu",
    "bhumlichok": "mumlichok",
    "dadiguranshe": "dandiguranse",
    "dandagaun/dandaghar": "dandagaun",
    "dandagoun": "danda gaun",
    "dapcha kasikhanda n.p.": "dapcha chatraebangha",
    "dhola": "dhol",
    "dhunkharka": "dhungkharka bahrabisae",
    "dudbhanjyang": "dudhbhanjyang",
    "dhussa": "dhursa",
    "dudhouli n.p.": "dudhauli",
    "dhuseni siwalaya": "ghusenisiwalaye",
    "dubachour": "dubachaur",
    "dhuskun": "gunsakun",
    "fakhel": "phakhel",
    "farpu": "tharpu",
    "falate bhumlu": "phalete",
    "falemetar": "phalametar",
    "faparbari": "phaparbari",
    "fasku": "phasku",
    "fediguth": "phedighooth",
    "fikuri": "phikuri",
    "foksingtar": "phoksingtar",
    "fujel": "phujel",
    "fulbari": "phulbari",
    "fulpingdanda": "phulchodanda",
    "fulpingkatti": "phulpingkatti",
    "fulpingkot": "phulpingkot",
    "gairi bisouna deupur": "gairi bisauna deupur",
    "ganeshthan": "ganeshsthan",
    "gankhu": "gangkhu",
    "gauri sankar": "gaurishankar",
    "ghyang sukathokar": "ghyangsukathokar",
    "gloche": "golche",
    "gogane": "gomane",
    "goswara": "goshwara",
    "gothpani": "gotpani",
    "gunsakot": "ghunsakot",
    "gunsi bhadaure": "gunsi",
    "gupteshwor": "gupteshwar",
    "halde kalika": "kalika halldae",
    "hariharpur gadhi": "hariharpurgadhi",
    "jalkanya": "jalkanyachapauli",
    "jeewanpur": "jiwanpur",
    "jhyaku": "jhyanku",
    "jiri n.p.": "jiri",
    "jyamrung": "jyamere",
    "kabhre": "kabre",
    "kalati bhumidanda": "kapali bhumaedanda",
    "kalinchowk": "kalinchok",
    "kalpabrishykha": "kalpabrikshya",
    "kamalamai municipality": "kamalami municipality",
    "kankada": "kangkada",
    "katike deurali": "kattike deurali",
    "katunje besi": "katunjebesi",
    "kerauja": "keroja",
    "kewalpur": "kebalpur",
    "khadag bhanjyang": "khadga bhanjyang",
    "khahare pangu": "khaharepangu",
    "khang sang": "khangsang",
    "khiji chandeshwori": "khijichandeshwori",
    "khigikati": "khijikaanthi",
    "khijifalate": "khijiflate",
    "kimtang": "kintang",
    "kiwool": "kiul",
    "kogate": "kagate",
    "kolati bhumlu": "kolanti",
    "kubukasthali": "kunbhukasthali",
    "kuibhir": "kuebhire",
    "kuseswor dumja": "kusheshwar dumja",
    "kyaneshwor": "kyaneshwar",
    "laharepouwa": "laharepauwa",
    "langarche": "lagarche",
    "lisankhu": "lisangkhu",
    "listikot": "listokot",
    "madan kundari": "madankundari",
    "madhavpur": "madhabpur",
    "mahadevdada": "mahadevdanda",
    "mahankal": "mahangkal",
    "mahankal chaur": "mahangkal",
    "mahendra jhyadi": "mahendrajhyadi",
    "mahendra jyoti": "mahendrajyoti bansdol",
    "majhi feda": "majhipheda",
    "makadum": "makadhum",
    "makwanpurgadhi": "makawanpur gadhi",
    "maneswnara": "maneswar",
    "mankha": "mangkha",
    "melamchi n.p.": "melamchi",
    "muchhok": "muchchok",
    "nagre gagarche": "nangregagarche",
    "namdu": "namdru",
    "mirkot": "virkot",
    "narmedeshwor": "narmadeshwor",
    "nayagaun deupur": "naya gaun deupur",
    "ramechhap municipality": "ramechhap",
    "manthali municipality": "ramechhap",
    "nilkantha n.p.": "nilkanth",
    "orang": "worang",
    "pagretar": "pangretar",
    "palte": "patale",
    "palungtar municipality": "palungtar",
    "patalekhet": "phalete",
    "panchkhal n.p.": "panchkhal",
    "petaku": "pedku",
    "pinkhuri": "phikuri",
    "pritee": "priti",
    "purano jhangajholi": "puranojhangajholi",
    "raniban": "ranibaan",
    "ratanchura": "ratanchur",
    "ravi opi": "rabiopi",
    "rawadolu": "rawadol",
    "ree gaun": "rigaun",
    "ryale": "ryale bihawar",
    "sahare": "shahare",
    "sailungeswor": "sailungeshwar",
    "salle bhumlu": "salle blullu",
    "salmechakala (taldhunga)": "salme taldhunga",
    "salyantar": "salyan tar",
    "samundradevi": "samudradevi kholegaun",
    "samundratar": "samudratar",
    "sanghutar": "sandhutar",
    "sankhupati chour": "sangkhupatichaur",
    "sanowangthali": "sanuwangthali",
    "santeswori (rampur)": "shanteshwari rampur",
    "sarada batase": "sharada (batase)",
    "sarasyunkhark": "sarsyunkharka",
    "sarikhet palase": "sarikhetpalase",
    "saurpani": "sairpani",
    "semjong": "semdhung",
    "serna": "sherma",
    "shreechaur": "shrichaor",
    "shreenathkot": "shrithankot",
    "shreepur chhatiwan": "shripur chhatiwan",
    "simalchour syampati": "syampati simalchaur",
    "sipa pokhare": "sipapokharae",
    "sipal kavre": "sinpal kavre",
    "sirthouli": "sirthauli",
    "sunam pokhari": "sumnampokhari",
    "syafru": "syaphru",
    "solpathana": "swalpathana",
    "sidhdhicharan n.p.": "shishneri",
    "sikhar ambote": "shikhar ambote",
    "sisneri mahadevsthan": "shishneri",
    "syaule bazar": "syaule",
    "takukot": "tarkukot",
    "takumajh lakuribot": "takumaj hlakuri",
    "taluwa": "tuluwa",
    "thampal chhap": "thumpakhar",
    "thangpalkot": "thapalkot",
    "thumi": "thumo",
    "tistung deurali": "tistung",
    "tokarpur": "thokarpur",
    "toksel": "tokshel",
    "tosramkhola": "tosarangkhola",
    "tukucha nala": "tukuchanala",
    "ugrachandi nala": "ugrachandinala",
    "yamunadanda": "yamuna danda",
    "yasam": "yesham",
    "thulachhap": "thulachaap",
    "thulogoun": "thulo gaun",
    "thum pakhar": "thumpakhar",
    "dhaibung": "jibjibe (nilkantha)",
    "dhumthang": "sindhukot",
    "dhunwakot": "ghunsakot",
    "gorkha municipality": "prithbinarayan municipality",
    "majuwa": "makadhum",
    "pokhari chauri": "chauri pokhari",
    "tamchet dudhpokhari": "dodhapokhari",
    "thaha n.p.": "bhimphedi"
}
#%% recode village names to match shapefile identifier
main_dists_eq['vdc'].replace(remaps, inplace=True)
#%% numerical bounds on income
incomes = {
    'Rs. 10 thousand':5000,
    'Rs. 10-20 thousand':15000,
    'Rs. 20-30 thousand':25000,
    'Rs. 30-50 thousand':40000,
    'Rs. 50 thousand or more':60000
}
main_dists_eq.loc[:,'income'] = main_dists_eq['income_level_household'].map(incomes)
#%%
main_dists_eq.income.describe()

#%%
%cd $dataroot
main_dists_eq.to_csv('hh_worst_hit.csv')
#%% crosswalk for building file
village_counts = main_dists_eq.groupby(
    ['District','District_Name','vdcmun_id','vdc']
    ).size().reset_index().rename(columns={0:'nvillages'})

village_counts.to_csv(dataroot+'vcode_vname_xwalk.csv')
#%%
village_counts.nvillages.sum()
