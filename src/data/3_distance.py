import seaborn as sns
import pandas as pd
import pysal as ps
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import mstats
import statsmodels.api as sm
import statsmodels.formula.api as smf

get_ipython().run_line_magic('matplotlib', 'inline')
# run for jupyter notebook
from IPython.core.debugger import Tracer
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = 'all'
#%% directories
working = '/home/alal/Desktop/code/0_research/eq-svy-eda/'
code    = working + 'src/data/'
data    = working + 'data/interim/'
#%%
villages = gpd.read_file(data+'main_districts_village_shapes.shp')
#%%
villages.head()
#%%
